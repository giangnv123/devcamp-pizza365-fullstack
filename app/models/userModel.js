const mongoose = require("mongoose");

const userModel = new mongoose.Schema({
  fullname: {
    type: String,
    required: [true, "User must have a full name"],
  },
  email: {
    type: String,
    required: [true, "User must have an email"],
    unique: [true, "Email address already exists"],
  },
  address: {
    type: String,
    required: [true, "User must have an address"],
  },
  phone: {
    type: String,
    required: [true, "User must have a phone number"],
  },
  orders: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Order",
    },
  ],
});

const User = mongoose.model("User", userModel);

module.exports = User;
