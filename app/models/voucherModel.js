const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const voucherSchema = new Schema({
  voucherCode: {
    type: String,
    require: [true, "Voucher must have a code!"],
    unique: [true, "Voucher code must be unique!"],
  },
  discount: {
    type: Number,
    require: [true, "Voucher must have a discount percent!"],
  },
  message: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

const Voucher = mongoose.model("Voucher", voucherSchema);

module.exports = Voucher;
