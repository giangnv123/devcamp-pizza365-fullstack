const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const drinkSchema = new Schema({
  drinkCode: {
    type: String,
    required: [true, "Please Provide drink code"],
    unique: [true, "Existing drink code!"],
  },
  drinkName: {
    type: String,
    required: [true, "Please provide drink name"],
  },
  price: {
    type: Number,
    required: [true, "Drink must have a price"],
  },
});

const Drink = mongoose.model("Drink", drinkSchema);

module.exports = Drink;
