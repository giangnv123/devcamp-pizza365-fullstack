const mongoose = require("mongoose");

// Define the Order schema

const orderSchema = new mongoose.Schema({
  orderCode: {
    type: String,
    unique: true,
    required: [true, "Order must have order code!"],
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, "Order must have user id!"],
  },
  email: {
    type: String,
    required: [true, "Order must have user email!"],
  },
  phone: {
    type: String,
    required: [true, "Order must have user phone number!"],
  },
  fullname: {
    type: String,
    required: [true, "Order must have user's name!"],
  },
  address: {
    type: String,
    required: [true, "Order must have user's address!"],
  },
  pizzaSize: {
    type: String,
    required: [true, "Please provide the pizza size"],
  },
  pizzaType: {
    type: String,
    required: [true, "Please provide the pizza type"],
  },
  message: {
    type: String,
    required: false,
  },
  finalPrice: {
    type: Number,
    required: [true, "Please provide final price!"],
  },
  voucherCode: {
    type: String,
    default: "",
    ref: "Voucher",
  },
  drinkId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Drink",
    default: mongoose.Types.ObjectId,
  },
  status: {
    type: String,
    required: [true, "Please provide the order status"],
  },
});

// Create the Order model
const Order = mongoose.model("Order", orderSchema);

// Export the Order model
module.exports = Order;
