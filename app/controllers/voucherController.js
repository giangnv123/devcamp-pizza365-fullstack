const mongoose = require("mongoose");
const Voucher = require("../models/voucherModel");

// Get All Vouchers
exports.getVouchers = async (req, res) => {
  try {
    const vouchers = await Voucher.find();
    res.status(200).json({
      message: "Success",
      results: vouchers.length,
      vouchers,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all Vouchers!",
      error,
    });
  }
};

// GEt voucher by id
exports.getVoucherById = async (req, res) => {
  try {
    const voucherId = req.params.voucherId;

    const voucher = await Voucher.findOne({ _id: voucherId });

    if (!voucher)
      return res.status(404).json({
        message: "Voucher not found!",
      });

    res.status(200).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get Voucher!",
      error,
    });
  }
};

// Get Voucher
exports.getVoucherByVoucherCode = async (req, res) => {
  try {
    const voucherCode = req.params.voucherCode;

    const voucher = await Voucher.findOne({ voucherCode }).select("voucherCode discount -_id");

    if (!voucher)
      return res.status(404).json({
        message: "Voucher not found!",
      });

    res.status(200).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get Voucher!",
      error,
    });
  }
};

// Update Voucher
exports.updateVoucher = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const voucher = await Voucher.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!voucher)
      return res.status(404).json({
        message: "Voucher not found!",
      });

    res.status(200).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update voucher.",
      error,
    });
  }
};

// Create Voucher
exports.createVoucher = async (req, res) => {
  try {
    const vouchers = await Voucher.find();

    let voucherCode = vouchers.length ? String(vouchers.at(-1).voucherCode * 1 + 1) : String(Date.now()).slice(3, 9);

    console.log(voucherCode);

    const newVoucher = { ...req.body, voucherCode };

    const voucher = await Voucher.create(newVoucher);

    res.status(201).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create Voucher!",
      error,
    });
  }
};

// Delete Voucher

exports.deleteVoucher = async (req, res) => {
  try {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const voucher = await Voucher.findByIdAndDelete(id);

    if (!voucher)
      return res.status(404).json({
        message: "Voucher not found!",
      });

    res.status(204).json({
      message: "Success",
      voucher,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete Voucher!",
      error,
    });
  }
};
