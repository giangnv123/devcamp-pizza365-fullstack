const mongoose = require("mongoose");
const Drink = require("../models/drinkModel");

// Get All Drinks
exports.getDrinks = async (req, res) => {
  try {
    let condition = {};
    const userId = req.query.userId;
    if (userId) condition.user = userId;

    const drinks = await Drink.find(condition).select("-__v");
    res.status(200).json({
      message: "Success",
      results: drinks.length,
      drinks,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all Drinks!",
      error,
    });
  }
};

// Get Drink
exports.getDrink = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const drink = await Drink.findById(req.params.id);

    if (!drink)
      return res.status(404).json({
        message: "Drink not found!",
      });

    res.status(200).json({
      message: "Success",
      drink,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get Drink!",
      error,
    });
  }
};

// Update Drink
exports.updateDrink = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const drink = await Drink.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!drink)
      return res.status(404).json({
        message: "Drink not found!",
      });

    res.status(200).json({
      message: "Success",
      drink,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update drink.",
      error,
    });
  }
};

// Create Drink
exports.createDrink = async (req, res) => {
  try {
    const drink = await Drink.create(req.body);

    res.status(201).json({
      message: "Success",
      drink,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create Drink!",
      error,
    });
  }
};

// Delete Drink

exports.deleteDrink = async (req, res) => {
  try {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const drink = await Drink.findByIdAndDelete(id);

    if (!drink)
      return res.status(404).json({
        message: "Drink not found!",
      });

    res.status(204).json({
      message: "Success",
      drink,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete Drink!",
      error,
    });
  }
};
