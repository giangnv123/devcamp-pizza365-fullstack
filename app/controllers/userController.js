const mongoose = require("mongoose");
const User = require("../models/userModel");

// Get All Users
exports.getUsers = async (req, res) => {
  try {
    let skip = req.query.skip ? Number(req.query.skip) : "";

    let limit = req.query.limit ? Number(req.query.limit) : "";

    let sort = req.query.sort ? req.query.sort.split(",").join(" ") : "";

    let condition = {};
    const userId = req.query.id;
    if (userId) condition.user = userId;

    const users = await User.find(condition).skip(skip).limit(limit).sort(sort);

    // const users = await User.aggregate([
    //   {
    //     $unwind: "$orders",
    //   },
    //   {
    //     $match: {
    //       fullname: "Faker",
    //     },
    //   },
    // ]);

    res.status(200).json({
      message: "Success",
      results: users.length,
      users,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get all User!",
      error,
    });
  }
};

// Get User
exports.getUser = async (req, res) => {
  try {
    const id = req.params.id;
    console.log(id, "id");
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const user = await User.findById(req.params.id);
    console.log(user, "user");
    if (!user)
      return res.status(404).json({
        message: "User not found!",
      });

    res.status(200).json({
      message: "Success",
      user,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to get User!",
      error,
    });
  }
};

// Update User
exports.updateUser = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!user)
      return res.status(404).json({
        message: "User not found!",
      });

    res.status(200).json({
      message: "Success",
      user,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update user.",
      error,
    });
  }
};

// Create User
exports.createUser = async (req, res) => {
  try {
    const user = await User.create(req.body);

    res.status(201).json({
      message: "Success",
      user,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create User!",
      error,
    });
  }
};

// Delete User

exports.deleteUser = async (req, res) => {
  try {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const user = await User.findByIdAndDelete(id);

    if (!user)
      return res.status(404).json({
        message: "User not found!",
      });

    res.status(204).json({
      message: "Success",
      user,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to delete User!",
      error,
    });
  }
};
