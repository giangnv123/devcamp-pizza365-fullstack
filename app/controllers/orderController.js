const mongoose = require("mongoose");
const Order = require("../models/orderModel");
const User = require("../models/userModel");
const Drink = require("../models/drinkModel");

// Get All Orders
exports.getOrders = async (req, res) => {
  try {
    let condition = {};
    const userId = req.query.userId;
    if (userId) condition.user = userId;

    const orders = await Order.find(condition).sort("-_id");
    res.status(200).json({
      message: "Success",
      results: orders.length,
      orders,
    });
  } catch (error) {
    res.status(500).json({
      message: "Fail to get all Orders!",
      error,
    });
  }
};

exports.getOrderOfUser = async (req, res) => {
  try {
    const userId = req.params.userId;

    let condition = {};
    if (userId) condition.userId = userId;
    const allOrders = await Order.find(condition);

    if (!allOrders.length)
      return res.status(404).json({
        message: "Order not found!",
      });

    res.status(200).json({
      message: "Success",
      results: allOrders.length,
      allOrders,
    });
  } catch (error) {
    res.status(500).json({
      message: "Fail to get Order!",
      error,
    });
  }
};

exports.getOrderByUserId = async (req, res) => {
  try {
    let condition = {};
    const userId = req.query.userId;
    if (userId) condition.user = userId;

    const orders = await Order.find(condition);
    res.status(200).json({
      message: "Success",
      results: orders.length,
      orders,
    });
  } catch (error) {
    res.status(500).json({
      message: "Fail to get all Orders!",
      error,
    });
  }
};

// Get Order
exports.getOrder = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const order = await Order.findOne({ _id: id });

    console.log(order, "order");

    if (!order)
      return res.status(404).json({
        message: "Order not found!",
      });

    res.status(200).json({
      message: "Success",
      order,
    });
  } catch (error) {
    res.status(500).json({
      message: "Fail to get Order!",
      error,
    });
  }
};

// Update Order
exports.updateOrder = async (req, res) => {
  try {
    const id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }
    const testOrder = await Order.findOne({ _id: id });
    console.log(testOrder, "test Order");
    const order = await Order.findByIdAndUpdate(id, req.body, {
      new: true,
      runValidators: true,
    });

    console.log(id, "id");
    console.log(order, " oreder");
    console.log(req.body, "req.body");

    if (!order)
      return res.status(404).json({
        message: "Order not found!",
      });

    res.status(200).json({
      message: "Success",
      order,
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to update order.",
      error,
    });
  }
};

// Create Order
exports.createOrder = async (req, res) => {
  try {
    // 1) Check Existing User, if user does not exist -> Create new one
    const userEmail = req.body.email;

    let user = await User.findOne({ email: userEmail });
    if (!user) {
      user = await User.create({
        email: userEmail,
        address: req.body.address,
        fullname: req.body.fullname,
        phone: req.body.phone,
      });
    }

    // 2) Get Drink Id - find drink Id
    const drink = await Drink.findOne({ drinkCode: req.body.drinkCode });

    // 4) Create new Order
    const newOrderObj = {
      ...req.body,
      orderCode: Date.now(),
      userId: user.id,
      drinkId: drink.id,
      voucherCode: req.body.voucherCode,
      status: "Pending",
    };

    const order = await Order.create(newOrderObj);

    // 5) Update Order Id Of User, drink Id and voucher Code
    await User.findByIdAndUpdate(user.id, {
      orders: [...user.orders, order.id],
    });

    res.status(201).json({
      message: "Success",
      order,
    });
  } catch (error) {
    res.status(500).json({
      message: "Fail to create Order!",
      error,
    });
  }
};

// Delete Order

exports.deleteOrder = async (req, res) => {
  try {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({
        message: "Invalid Id Type!",
      });
    }

    const order = await Order.findByIdAndDelete(id);

    if (!order)
      return res.status(404).json({
        message: "Order not found!",
      });

    res.status(204).json({
      message: "Success",
      order,
    });
  } catch (error) {
    res.status(500).json({
      message: "Fail to delete Order!",
      error,
    });
  }
};

exports.deleteAllOrder = async (req, res) => {
  try {
    await Order.deleteMany();

    res.status(200).json({
      message: "Delete All Orders Successfully!",
    });
  } catch (error) {
    res.status(500).json({
      message: "Fail to delete Order!",
      error,
    });
  }
};
