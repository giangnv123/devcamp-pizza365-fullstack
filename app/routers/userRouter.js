const express = require("express");

const { getUsers, getUser, createUser, updateUser, deleteUser } = require("../controllers/userController");

// Create router
const userRouter = express.Router();

/***** ROUTER *****/

userRouter.route("/users").get(getUsers).post(createUser);

userRouter.route("/users/:id").put(updateUser).get(getUser).delete(deleteUser);

// Export user Router
module.exports = { userRouter };
