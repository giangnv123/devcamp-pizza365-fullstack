const express = require("express");

const {
  getVouchers,
  getVoucherByVoucherCode,
  createVoucher,
  updateVoucher,
  deleteVoucher,
  getVoucherById,
} = require("../controllers/voucherController");

// Create router
const voucherRouter = express.Router();

/***** ROUTER *****/
voucherRouter.route("/vouchers/:voucherId").get(getVoucherById);

voucherRouter.route("/vouchers").get(getVouchers).post(createVoucher);

voucherRouter.route("/vouchers/:id").put(updateVoucher).delete(deleteVoucher);
voucherRouter.route("/vouchers/:voucherCode").get(getVoucherByVoucherCode);

// Export voucher Router
module.exports = { voucherRouter };
