const express = require("express");

const { getDrinks, getDrink, createDrink, updateDrink, deleteDrink } = require("../controllers/drinkController");

/***** ROUTER *****/

// Create router
const drinkRouter = express.Router();

drinkRouter.route("/drinks").get(getDrinks).post(createDrink);

drinkRouter.route("/drinks/:id").patch(updateDrink).get(getDrink).delete(deleteDrink);

// Export drink Router
module.exports = { drinkRouter };
