const express = require("express");

const {
  getOrders,
  getOrder,
  createOrder,
  updateOrder,
  deleteOrder,
  getOrderOfUser,
  deleteAllOrder,
} = require("../controllers/orderController");

// Create router
const orderRouter = express.Router({ mergeParams: true });

/***** ROUTER *****/
orderRouter.route("/orders/:id").put(updateOrder).get(getOrder).delete(deleteOrder);

orderRouter.route("/orders/delete-all").delete(deleteAllOrder);

orderRouter.route("/orders").get(getOrders).post(createOrder);

orderRouter.route("/users/:userid/orders").get(getOrderOfUser);

// Export order Router
module.exports = { orderRouter };
