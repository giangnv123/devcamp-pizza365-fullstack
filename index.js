const mongoose = require("mongoose");
const express = require("express");
const app = express();
const path = require("path");

// Get Routers
const { drinkRouter } = require("./app/routers/drinkRouter");
const { voucherRouter } = require("./app/routers/voucherRouter");
const { userRouter } = require("./app/routers/userRouter");
const { orderRouter } = require("./app/routers/orderRouter");

const port = 8000;

// View User Page
app.use(express.static(__dirname + "/views"));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + "/views/pizza365.html"));
});
app.use(express.json());

// Connect to mongoDb Local Database
const collection = `CRUD_Devcamp-Pizza365-test`;
const mongoUrl = `mongodb://127.0.0.1:27017/`;
mongoose
  .connect(mongoUrl + collection)
  .then(() => console.log(`DB Connection to Local Database (${collection}) Successfully!`));

app.use("/api/v1/devcamp-pizza365", drinkRouter);
app.use("/api/v1/devcamp-pizza365", voucherRouter);
app.use("/api/v1/devcamp-pizza365", userRouter);
app.use("/api/v1/devcamp-pizza365", orderRouter);

app.listen(port, () => console.log(`App is running on port: ${port}`));

module.exports = app;
