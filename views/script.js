"use strict";

const urlOrder = "api/v1/devcamp-pizza365/orders";

const orderContainer = $("#div-container-order");

// All Pizza size property
const PIZZA_SIZE_OBJ = {
  S: {
    pizzaSize: "S",
    diameter: 20,
    babecue: 2,
    salad: 200,
    drinkQuantity: 2,
    price: 150000,
  },
  M: {
    pizzaSize: "M",
    diameter: 25,
    babecue: 4,
    salad: 300,
    drinkQuantity: 3,
    price: 200000,
  },
  L: {
    pizzaSize: "L",
    diameter: 30,
    babecue: 8,
    salad: 500,
    drinkQuantity: 4,
    price: 250000,
  },
};

let pizzaSizeObj = null;
let pizzaType = null;
let drinkCode = null;
let userOrderObj = null;

// Event handle to select pizza size
$("#pizza-size-container").click((e) => {
  if (!e.target.classList.contains("btn-choose-size")) return;
  $(".btn-choose-size").removeClass("btn-yellow");
  e.target.classList.add("btn-yellow");

  pizzaSizeObj = PIZZA_SIZE_OBJ[e.target.dataset.size];

  console.log(`%cYou've selected a Pizza Size ${pizzaSizeObj.size}`, `color:Pink`);
});

// Event handle to select pizza type
$("#pizza-type-container").click((e) => {
  if (!e.target.classList.contains("btn-choose-type")) return;

  $(".btn-choose-type").removeClass("btn-yellow");
  e.target.classList.add("btn-yellow");

  pizzaType = e.target.dataset.type;

  console.log(`%cYou've selected a ${pizzaType} Pizza `, `color: Yellow`);
});

// Fun
async function onButtonClickToCheckOrder() {
  if (!pizzaSizeObj || !pizzaType || drinkCode == "0" || !drinkCode) {
    alert("Please select Pizza Size, Pizza Type and Drink");
    return;
  }

  let orderObj = {
    pizzaType: "",
    drinkCode: "", // add drink
    fullname: "",
    email: "",
    phone: "",
    address: "",
    pizzaSize: "",
    price: 0,
    diameter: 0,
    babecue: 0,
    salad: 0,
    drinkQuantity: 0,
    discount: 0,
    message: "",
    voucherCode: "",
    discountPrice: function () {
      if (!this.voucherCode) return this.price; // if user don't add voucherCode - return original Price
      return this.price * ((100 - this.discount) / 100); //  user have voucherCode - calculate discount price
    },
  };

  // handle voucher discount percent
  let voucherValue = $("#input-voucher").val().trim();
  orderObj.discount = await callApiToGetDisCountPercent(voucherValue);
  if (voucherValue && orderObj.discount === 0) {
    alert("Please enter correct voucher or leave this field empty!");
    return;
  }

  updateDataToOrderObj(orderObj);

  if (!validateAllInput(orderObj)) return;
  userOrderObj = orderObj; // Update global user Order Obj

  displayOrderInfor(orderObj);

  submitBtn.css("display", "block");
  orderContainer.removeClass("bg-warning").addClass("bg-info");
  $("#div-container-infor").addClass("text-white");
}

$("#select-drink").click((e) => {
  // If user don't choose drink, then stop function
  if (e.target.tagName === "SELECT") {
    drinkCode = e.target.value;
  }
});

function updateDataToOrderObj(paramOrderObj) {
  let vMenuKeys = Object.keys(pizzaSizeObj);
  vMenuKeys.forEach((key) => (paramOrderObj[key] = pizzaSizeObj[key]));

  paramOrderObj.pizzaType = pizzaType;
  paramOrderObj.fullname = $("#input-name").val().trim();
  paramOrderObj.email = $("#input-email").val().trim();
  paramOrderObj.phone = $("#input-phone").val().trim();
  paramOrderObj.address = $("#input-address").val().trim();
  paramOrderObj.message = $("#input-message").val().trim();
  paramOrderObj.voucherCode = $("#input-voucher").val().trim();
  paramOrderObj.drinkCode = $("#select-drink").val();
}

// Function to validate all input
function validateAllInput(paramOrder) {
  if (!paramOrder.email || !paramOrder.fullname || !paramOrder.phone || !paramOrder.address || !paramOrder.message)
    alert("Please field out all inputs! Voucher is optional!");
  else if (!validateEmail(paramOrder.email)) {
    alert("Please enter valid Email!");
  } else if (!validatePhoneNumber(paramOrder.phone)) {
    alert("Please enter correct phone number");
  } else {
    return true;
  }
  return false;
}

// Function to call Api to get discount percent
async function callApiToGetDisCountPercent(paramVoucher) {
  let discountPercent = 0;
  try {
    const urlVoucher = "api/v1/devcamp-pizza365/vouchers";

    if (!paramVoucher) return discountPercent;

    let vResponse = await fetch(`${urlVoucher}/${paramVoucher}`);

    if (!vResponse.ok) {
      throw new Error(`Cannot Get Voucher! ${vResponse.status}`);
    }
    let data = await vResponse.json();

    discountPercent = data.voucher ? data.voucher.discount : 0;
  } catch (error) {
    console.log(error.message);
  }
  return discountPercent;
}

$("#check-order-button").click(onButtonClickToCheckOrder);

function displayOrderInfor(paramOrderObj) {
  orderContainer.css({
    display: "block",
    backgroundColor: "green",
  });
  let html = `<p>Fullname: ${paramOrderObj.fullname}</p>
              <p>Email: ${paramOrderObj.email}</p>
              <p>Phone Number: ${paramOrderObj.phone}</p>
              <p>Address: ${paramOrderObj.address}</p>
              <p>Message: ${paramOrderObj.message}</p>
              <hr/>
              <p>Size: ${paramOrderObj.pizzaSize}</p>
              <p>Diameter: ${paramOrderObj.diameter} cm</p>
              <p>Babecue Pork: ${paramOrderObj.babecue} Pieces</p>
              <p>Salad: ${paramOrderObj.salad} gram</p>
              <p>Drink quantity: ${paramOrderObj.drinkQuantity} </p>
              <p>Drink: ${paramOrderObj.drinkCode} </p>
              <hr/>
              <p>Pizza Type: ${paramOrderObj.pizzaType}</p>
              <p>Voucher ID: ${paramOrderObj.voucherCode}</p>
              <p>Price: ${paramOrderObj.price} VND</p>
              <p>Discount: ${paramOrderObj.discount}%</p>
              <p>Final Cost: ${paramOrderObj.discountPrice()} VND</p>
              `;
  $("#div-order-infor").html(html); // Display order data to html
}

// POST ORDER
let submitBtn = $("#btn-submit");
async function callApiToPostOrder() {
  userOrderObj = { ...userOrderObj, finalPrice: userOrderObj.discountPrice() };

  console.log(userOrderObj);

  try {
    let vResponse = await fetch(urlOrder, {
      method: "POST",
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
      },
      body: JSON.stringify(userOrderObj),
    });

    if (!vResponse.ok) {
      console.log("Fail to post on server side!");
      throw new Error(`HTTP ERROR! ${vResponse.status}`);
    } else {
      console.log("POST new order Successufully");
      let newOrder = await vResponse.json();
      return newOrder.order.orderCode;
    }
  } catch (error) {
    console.log(error.message);
  }
}

orderContainer.css("display", "none");
async function handleSubmitBtn() {
  let orderCode = await callApiToPostOrder();

  orderContainer.removeClass("bg-info").addClass("bg-warning");

  $("#div-order-infor")
    .addClass("show-border-info")
    .html(`<p>Order Successfully! <br>Your Order Code is: ${orderCode}</p>`);

  submitBtn.css("display", "none");
}

submitBtn.click(handleSubmitBtn);

/************** Sub Function **************/
// Function to add drink list to browser
(async function callApiToGetDrinks() {
  const urlDrinks = "api/v1/devcamp-pizza365/drinks/";

  let vResponse = await fetch(urlDrinks);
  try {
    if (!vResponse.ok) {
      throw new Error(`HTTP ERROR! ${vResponse.status}`);
    }
    let data = await vResponse.json();
    addDrinkList(data.drinks);
  } catch (error) {
    console.log(error.message);
  }
})();

function addDrinkList(drinkList) {
  let html = `<option class="drink" value="0">Select a drink</option>`;
  drinkList.forEach((drink) => {
    html += `<option class="drink" value="${drink.drinkCode}">${drink.drinkName}</option>`;
  });
  $("#select-drink").html(html);
}

/************** Additional Function **************/

// Function to validate email
function validateEmail(paramEmail) {
  let vValidRegex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (paramEmail.match(vValidRegex)) {
    return true;
  } else {
    return false;
  }
}

// Function to check phone number
function validatePhoneNumber(phoneNumber) {
  if (isNaN(phoneNumber)) {
    alert("Phone number must be only number!");
    return false;
  }

  if (phoneNumber.length < 10 || phoneNumber.length > 12) {
    alert("Phone number length must be from 9 to 11");

    return false;
  }

  if (phoneNumber.charAt(0) !== "0") {
    alert("The First number must be 0");

    return false;
  }

  return true;
}
