const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
const expect = require("chai").expect;
const should = chai.should();
const mongoose = require("mongoose");
const mongoUrl = `mongodb://127.0.0.1:27017/CRUD_Devcamp-Pizza365-test`;

chai.use(chaiHttp);

describe("Test Voucher API", () => {
  it("should connect and disconnect to mongodb", async () => {
    // console.log(mongoose.connection.states);
    mongoose.disconnect();
    mongoose.connection.on("disconnected", () => {
      expect(mongoose.connection.readyState).to.equal(0);
    });
    mongoose.connection.on("connected", () => {
      expect(mongoose.connection.readyState).to.equal(1);
    });
    mongoose.connection.on("error", () => {
      expect(mongoose.connection.readyState).to.equal(99);
    });

    await mongoose.connect(mongoUrl);
  });

  // Create Voucher
  describe("Create Voucher (POST)", () => {
    it("should create a new voucher", async () => {
      const newVoucher = {
        discount: 19,
      };

      const res = await chai.request(server).post("/api/v1/devcamp-pizza365/vouchers").send(newVoucher);

      res.should.have.status(201);
      res.body.voucher.should.be.a("object");
      res.body.voucher.should.have.property("discount", 19);

      // Add any additional assertions you want to make about the created voucher object
    });
  });

  // Get ALl Voucher
  describe("Get All Vouchers", () => {
    it("Should get All Vouchers", async () => {
      const res = await chai.request(server).get("/api/v1/devcamp-pizza365/vouchers");

      res.should.have.status(200);
      res.body.vouchers.should.be.an("array");
    });
  });

  // Get Voucher By Id
  describe("Get Voucher By Id (Get)", () => {
    it("Should Get A Voucher By Id", async () => {
      // Create a voucher
      const newVoucher = {
        discount: 12,
      };

      const createdVoucher = await chai.request(server).post("/api/v1/devcamp-pizza365/vouchers").send(newVoucher);
      // Get Voucher by id
      const voucherId = createdVoucher.body.voucher._id;

      const res = await chai.request(server).get(`/api/v1/devcamp-pizza365/vouchers/${voucherId}`);

      // Test
      res.should.have.status(200);
      res.body.voucher.should.have.property("discount", 12);
    });
  });

  // Update Voucher By Id
  describe("Update Voucher By Id (PUT)", () => {
    it("should update a voucher by id", async () => {
      // First, create a voucher to update
      const newVoucher = {
        discount: 15,
      };
      const createdVoucher = await chai.request(server).post("/api/v1/devcamp-pizza365/vouchers").send(newVoucher);

      console.log(createdVoucher.body.voucher, " voucher body voucher");
      const voucherId = createdVoucher.body.voucher._id;
      console.log(voucherId);

      // Update voucher with new values
      const updatedVoucher = {
        discount: 20,
      };
      const res = await chai.request(server).put(`/api/v1/devcamp-pizza365/vouchers/${voucherId}`).send(updatedVoucher);

      res.should.have.status(200);
      res.body.voucher.should.have.property("discount", 20);

      // Additional assertions if needed
    });
  });

  // Delete Voucher By Id
  describe("Delete Voucher By Id (DELETE)", () => {
    it("should delete a voucher by id", async () => {
      // First, create a voucher to delete
      const newVoucher = {
        discount: 18,
      };
      const createdVoucher = await chai.request(server).post(`/api/v1/devcamp-pizza365/vouchers`).send(newVoucher);
      const voucherId = createdVoucher.body.voucher._id;

      // Delete voucher
      const res = await chai.request(server).delete(`/api/v1/devcamp-pizza365/vouchers/${voucherId}`);

      res.should.have.status(204);
    });
  });
});
