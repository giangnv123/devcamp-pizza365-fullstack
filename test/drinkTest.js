const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
const expect = require("chai").expect;
const should = chai.should();
const mongoose = require("mongoose");
const mongoUrl = `mongodb://127.0.0.1:27017/CRUD_Devcamp-Pizza365-test`;

chai.use(chaiHttp);

describe("Test Drink API", () => {
  it("should connect and disconnect to mongodb", async () => {
    // console.log(mongoose.connection.states);
    mongoose.disconnect();
    mongoose.connection.on("disconnected", () => {
      expect(mongoose.connection.readyState).to.equal(0);
    });
    mongoose.connection.on("connected", () => {
      expect(mongoose.connection.readyState).to.equal(1);
    });
    mongoose.connection.on("error", () => {
      expect(mongoose.connection.readyState).to.equal(99);
    });

    await mongoose.connect(mongoUrl);
  });

  // Create Drink
  describe("Create Drink (POST)", () => {
    it("should create a new drink", async () => {
      const newDrink = {
        drinkCode: "464",
        drinkName: "Test Drink",
        price: 12000,
      };

      const res = await chai.request(server).post("/api/v1/devcamp-pizza365/drinks").send(newDrink);

      res.should.have.status(201);
      res.body.drink.should.be.a("object");
      res.body.drink.should.have.property("drinkCode", "464");
      res.body.drink.should.have.property("drinkName", "Test Drink");
      res.body.drink.should.have.property("price", 12000);

      // Add any additional assertions you want to make about the created drink object
    });
  });

  // Get ALl Drink
  describe("Get All Drinks", () => {
    it("Should get All Drinks", async () => {
      const res = await chai.request(server).get("/api/v1/devcamp-pizza365/drinks");

      res.should.have.status(200);
      res.body.drinks.should.be.an("array");
    });
  });

  // Get Drink By Id
  describe("Get Drink By Id (Get)", () => {
    it("Should Get A Drink By Id", async () => {
      // Create a drink
      const newDrink = {
        drinkCode: "6565",
        drinkName: "Test Drink",
        price: 12000,
      };

      const createdDrink = await chai.request(server).post("/api/v1/devcamp-pizza365/drinks").send(newDrink);
      // Get Drink by id
      const drinkId = createdDrink.body.drink._id;

      const res = await chai.request(server).get(`/api/v1/devcamp-pizza365/drinks/${drinkId}`);

      // Test
      res.should.have.status(200);
      res.body.drink.should.have.property("drinkCode", "6565");
      res.body.drink.should.have.property("drinkName", "Test Drink");
      res.body.drink.should.have.property("price", 12000);
    });
  });

  // Update Drink By Id
  describe("Update Drink By Id (PUT)", () => {
    it("should update a drink by id", async () => {
      // First, create a drink to update
      const newDrink = {
        drinkCode: "12312432423",
        drinkName: "Test Drink",
        price: 12000,
      };
      const createdDrink = await chai.request(server).post("/api/v1/devcamp-pizza365/drinks").send(newDrink);

      console.log(createdDrink.body.drink, " drink body drink");
      const drinkId = createdDrink.body.drink._id;
      console.log(drinkId);

      // Update drink with new values
      const updatedDrink = {
        price: 15000,
      };
      const res = await chai.request(server).patch(`/api/v1/devcamp-pizza365/drinks/${drinkId}`).send(updatedDrink);

      res.should.have.status(200);
      res.body.drink.should.have.property("drinkCode", "12312432423");
      res.body.drink.should.have.property("drinkName", "Test Drink");
      res.body.drink.should.have.property("price", 15000);

      // Additional assertions if needed
    });
  });

  // Delete Drink By Id
  describe("Delete Drink By Id (DELETE)", () => {
    it("should delete a drink by id", async () => {
      // First, create a drink to delete
      const newDrink = {
        drinkCode: "1232222",
        drinkName: "Test Drink",
        price: 12000,
      };
      const createdDrink = await chai.request(server).post(`/api/v1/devcamp-pizza365/drinks`).send(newDrink);
      const drinkId = createdDrink.body.drink._id;

      // Delete drink
      const res = await chai.request(server).delete(`/api/v1/devcamp-pizza365/drinks/${drinkId}`);

      res.should.have.status(204);
    });
  });
});
