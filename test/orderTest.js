const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
const expect = require("chai").expect;
const should = chai.should();
const mongoose = require("mongoose");
const mongoUrl = `mongodb://127.0.0.1:27017/CRUD_Devcamp-Pizza365-test`;
const Order = require("../app/models/orderModel");

chai.use(chaiHttp);

describe("Test Order API", () => {
  it("should connect and disconnect to mongodb", async () => {
    // console.log(mongoose.connection.states);
    mongoose.disconnect();
    mongoose.connection.on("disconnected", () => {
      expect(mongoose.connection.readyState).to.equal(0);
    });
    mongoose.connection.on("connected", () => {
      expect(mongoose.connection.readyState).to.equal(1);
    });
    mongoose.connection.on("error", () => {
      expect(mongoose.connection.readyState).to.equal(99);
    });

    await mongoose.connect(mongoUrl);
  });

  const newOrder = {
    address: "ahahah",
    diameter: 30,
    discount: 0,
    drinkCode: "TRATAC",
    drinkQuantity: 4,
    email: "sktasdsad@gmail.com",
    finalPrice: 250000,
    fullname: "Faker",
    message: "hahaha",
    phone: "0866778381",
    pizzaSize: "L",
    pizzaType: "bacon",
    price: 250000,
    salad: 500,
    voucherCode: "",
  };

  // Create Order
  describe("Create Order (POST)", () => {
    it("should create a new order", async () => {
      await Order.deleteMany();
      const res = await chai.request(server).post("/api/v1/devcamp-pizza365/orders").send(newOrder);

      res.should.have.status(201);
      res.body.order.should.be.a("object");
      res.body.order.should.have.property("address", "ahahah");

      // Add any additional assertions you want to make about the created order object
    });
  });

  // Get ALl Order
  describe("Get All Orders", () => {
    it("Should get All Orders", async () => {
      const res = await chai.request(server).get("/api/v1/devcamp-pizza365/orders");

      res.should.have.status(200);
      res.body.orders.should.be.an("array");
    });
  });

  // Get Order By Id
  describe("Get Order By Id (Get)", () => {
    it("Should Get A Order By Id", async () => {
      // Create a order

      const createdOrder = await chai.request(server).post("/api/v1/devcamp-pizza365/orders").send(newOrder);
      // Get Order by id
      const orderId = createdOrder.body.order._id;

      const res = await chai.request(server).get(`/api/v1/devcamp-pizza365/orders/${orderId}`);

      // Test
      res.should.have.status(200);
      res.body.order.should.be.a("object");
      res.body.order.should.have.property("address", "ahahah");
    });
  });

  // Update Order By Id
  describe("Update Order By Id (PUT)", () => {
    it("should update a order by id", async () => {
      const createdOrder = await chai.request(server).post("/api/v1/devcamp-pizza365/orders").send(newOrder);

      const orderId = createdOrder.body.order._id;
      console.log(orderId, " order id");

      // Update order with new values
      const updatedOrder = {
        pizzaType: "S",
      };
      const res = await chai.request(server).put(`/api/v1/devcamp-pizza365/orders/${orderId}`).send(updatedOrder);

      res.should.have.status(200);
      res.body.order.should.have.property("pizzaType", "S");

      // Additional assertions if needed
    });
  });

  // Delete Order By Id
  describe("Delete Order By Id (DELETE)", () => {
    it("should delete a order by id", async () => {
      // First, create a order to delete

      const createdOrder = await chai.request(server).post(`/api/v1/devcamp-pizza365/orders`).send(newOrder);
      const orderId = createdOrder.body.order._id;

      // Delete order
      const res = await chai.request(server).delete(`/api/v1/devcamp-pizza365/orders/${orderId}`);

      res.should.have.status(204);
    });
  });
});
