const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
const expect = require("chai").expect;
const should = chai.should();
const mongoose = require("mongoose");
const mongoUrl = `mongodb://127.0.0.1:27017/CRUD_Devcamp-Pizza365-test`;
const User = require("../app/models/userModel");

chai.use(chaiHttp);

describe("Test User API", () => {
  it("should connect and disconnect to mongodb", async () => {
    // console.log(mongoose.connection.states);
    mongoose.disconnect();
    mongoose.connection.on("disconnected", () => {
      expect(mongoose.connection.readyState).to.equal(0);
    });
    mongoose.connection.on("connected", () => {
      expect(mongoose.connection.readyState).to.equal(1);
    });
    mongoose.connection.on("error", () => {
      expect(mongoose.connection.readyState).to.equal(99);
    });

    await mongoose.connect(mongoUrl);
  });

  // Create User
  describe("Create User (POST)", () => {
    it("should create a new user", async () => {
      await User.deleteMany();

      const newUser = {
        fullname: "test User",
        address: "test address",
        phone: "01549461841",
        email: "testemail@gmail.com",
      };

      const res = await chai.request(server).post("/api/v1/devcamp-pizza365/users").send(newUser);

      res.should.have.status(201);
      res.body.user.should.be.a("object");
      res.body.user.should.have.property("fullname", "test User");
      res.body.user.should.have.property("address", "test address");
      res.body.user.should.have.property("phone", "01549461841");
      res.body.user.should.have.property("email", "testemail@gmail.com");

      // Add any additional assertions you want to make about the created user object
    });
  });

  // Get ALl User
  describe("Get All Users", () => {
    it("Should get All Users", async () => {
      const res = await chai.request(server).get("/api/v1/devcamp-pizza365/users");

      res.should.have.status(200);
      res.body.users.should.be.an("array");
    });
  });

  // Get User By Id
  describe("Get User By Id (Get)", () => {
    it("Should Get A User By Id", async () => {
      // Create a user
      const newUser = {
        fullname: "test User",
        address: "test address",
        phone: "01549461841",
        email: "testemailaaaa@gmail.com",
      };

      const createdUser = await chai.request(server).post("/api/v1/devcamp-pizza365/users").send(newUser);
      // Get User by id
      const userId = createdUser.body.user._id;

      const res = await chai.request(server).get(`/api/v1/devcamp-pizza365/users/${userId}`);

      // Test
      res.should.have.status(200);
      res.body.user.should.have.property("fullname", "test User");
      res.body.user.should.have.property("address", "test address");
      res.body.user.should.have.property("phone", "01549461841");
      res.body.user.should.have.property("email", "testemailaaaa@gmail.com");
    });
  });

  // Update User By Id
  describe("Update User By Id (PUT)", () => {
    it("should update a user by id", async () => {
      // First, create a user to update
      const newUser = {
        fullname: "test User",
        address: "test address",
        phone: "01549461841",
        email: "testemailxxx@gmail.com",
      };
      const createdUser = await chai.request(server).post("/api/v1/devcamp-pizza365/users").send(newUser);

      const userId = createdUser.body.user._id;

      // Update user with new values
      const updatedUser = {
        fullname: "test updated User",
        address: "test updated address",
        phone: "01549461842",
        email: "testupdatedemail@gmail.com",
      };
      const res = await chai.request(server).put(`/api/v1/devcamp-pizza365/users/${userId}`).send(updatedUser);

      res.should.have.status(200);
      res.body.user.should.have.property("fullname", "test updated User");
      res.body.user.should.have.property("address", "test updated address");
      res.body.user.should.have.property("phone", "01549461842");
      res.body.user.should.have.property("email", "testupdatedemail@gmail.com");

      // Additional assertions if needed
    });
  });

  // Delete User By Id
  describe("Delete User By Id (DELETE)", () => {
    it("should delete a user by id", async () => {
      // First, create a user to delete
      const newUser = {
        fullname: "test User",
        address: "test address",
        phone: "01549461841",
        email: "testemail@gmailzzzz.com",
      };
      const createdUser = await chai.request(server).post(`/api/v1/devcamp-pizza365/users`).send(newUser);
      const userId = createdUser.body.user._id;
      console.log(userId, "user id");

      // Delete user
      const res = await chai.request(server).delete(`/api/v1/devcamp-pizza365/users/${userId}`);

      res.should.have.status(204);
    });
  });
});
